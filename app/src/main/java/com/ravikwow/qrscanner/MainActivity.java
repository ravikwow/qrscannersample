package com.ravikwow.qrscanner;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.widget.Toast;

import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.MultiProcessor;
import com.google.android.gms.vision.Tracker;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    private CameraSource cameraSource;
    private CameraSourcePreview preview;
    private GraphicOverlay graphicOverlay;
    private Handler handler = new Handler();

    private MultiProcessor.Factory<Barcode> foodBarcodeTrackerFactory = new MultiProcessor.Factory<Barcode>() {
        @Override
        public Tracker<Barcode> create(Barcode barcode) {
            return new QRTracker();
        }
    };

    public static boolean checkCamera(Context context, int cameraType) {
        boolean isCameraExist = false;
        PackageManager pm = context.getPackageManager();
        if (pm.hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                CameraManager cameraManager = (CameraManager) context.getSystemService(Context.CAMERA_SERVICE);
                try {
                    for (String cameraId : cameraManager.getCameraIdList()) {
                        CameraCharacteristics cameraCharacteristics = cameraManager.getCameraCharacteristics(cameraId);
                        Integer type = cameraCharacteristics.get(CameraCharacteristics.LENS_FACING);
                        if (type != null && type == cameraType) {
                            isCameraExist = true;
                            break;
                        }
                    }
                } catch (Exception ignored) {
                }
            } else {
                try {
                    //noinspection deprecation
                    Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
                    //noinspection deprecation
                    for (int camIdx = 0; camIdx < Camera.getNumberOfCameras(); camIdx++) {
                        //noinspection deprecation
                        Camera.getCameraInfo(camIdx, cameraInfo);
                        //noinspection deprecation
                        if (cameraInfo.facing == cameraType) {
                            isCameraExist = true;
                            break;
                        }
                    }
                } catch (Exception ignored) {
                }
            }
        }
        return isCameraExist;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //noinspection deprecation
        if (!checkCamera(this, Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP ? CameraMetadata.LENS_FACING_BACK : Camera.CameraInfo.CAMERA_FACING_BACK)) {
            // TODO Camera don't exist
            finish();
            return;
        }
        setContentView(R.layout.activity_main);
        preview = (CameraSourcePreview) findViewById(R.id.preview);
        graphicOverlay = (GraphicOverlay) findViewById(R.id.graphicOverlay);
        setUpCamera();
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, 0);
    }

    @Override
    public void onResume() {
        super.onResume();
        startCamera();
    }

    @Override
    public void onPause() {
        super.onPause();
        preview.stop();
    }

    private void startCamera() {
        if (cameraSource != null) {
            try {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                preview.start(cameraSource, graphicOverlay);
            } catch (IOException e) {
                e.printStackTrace();
                cameraSource.release();
                cameraSource = null;
            }
        }
    }

    private void setUpCamera() {
        BarcodeDetector barcodeDetector = new BarcodeDetector.Builder(this).setBarcodeFormats(Barcode.QR_CODE).build();
        barcodeDetector.setProcessor(new MultiProcessor.Builder<>(foodBarcodeTrackerFactory).build());
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        CameraSource.Builder builder = new CameraSource.Builder(this, barcodeDetector)
                .setFacing(CameraSource.CAMERA_FACING_BACK)
                .setRequestedPreviewSize(displayMetrics.heightPixels, displayMetrics.widthPixels)
                .setRequestedFps(15.0f)
                .setAutoFocusEnabled(true);
        cameraSource = builder.build();
    }

    private class QRTracker extends Tracker<Barcode> {
        @Override
        public void onNewItem(int id, final Barcode item) {
            super.onNewItem(id, item);
            final String qrStr = item.rawValue;
            handler.post(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(MainActivity.this, qrStr, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}
